#ifndef THREAD_THREAD_POOL_HPP
#define THREAD_THREAD_POOL_HPP

#include <thread>
#include <queue>
#include <vector>
#include <functional>
#include <iostream>

namespace thread {
    namespace thread_pool {

        class ThreadPool {
        public:
            explicit ThreadPool(uint worker_count = std::thread::hardware_concurrency()) : worker_count_{ worker_count } {}

            ~ThreadPool() {
                Stop();
            }

            void AddJob(std::function<void()> job) {
                std::lock_guard<std::mutex> lg{ state_lock_ };
                work_queue_.push(job);
                jobs_cv_.notify_one();
            }

            void Start() {
                running_ = true;
                for (uint i = 0; i != worker_count_; ++i) {
                    threads_.emplace_back([this]() {
                        RunWorkerThread();
                        });
                }
            }

            void Stop() {
                running_.store(false);
                jobs_cv_.notify_all();

                for (auto& thread : threads_) {
                    std::cout << "Stopping thread " << thread.get_id() << '\n';
                    thread.join();
                }

                threads_.clear();
            }

            bool IsRunning() const {
                return running_.load();
            }

        private:

            void RunWorkerThread() {
                std::cout << "Starting thread " << std::this_thread::get_id() << '\n';
                while (running_) {
                    std::unique_lock<std::mutex> condition_lock{ state_lock_ };
                    jobs_cv_.wait(condition_lock, [this]() { return !running_ || !work_queue_.empty(); });

                    if (running_) {
                        auto function = work_queue_.front();
                        work_queue_.pop();

                        condition_lock.unlock();
                        std::cout << "Executing new function for " << std::this_thread::get_id() << '\n';
                        function();
                    }
                }
            }

            std::atomic_bool running_{ false };

            const uint worker_count_;
            std::vector<std::thread> threads_;

            std::mutex state_lock_;
            std::condition_variable jobs_cv_;
            std::queue<std::function<void()>> work_queue_;
        };


    }
}

#endif