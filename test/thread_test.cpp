#include "catch2/catch_all.hpp"

#include "thread/thread_pool.hpp"
#include <future>
#include <iostream>

namespace thread {
    namespace thread_pool {
        namespace {
            int DoWork(int start) {
                int out{ start + 1 };
                for (int i = 1; i < 10; ++i) {
                    out *= i;
                }
                return out;
            }
        }

        TEST_CASE("thread test") {
            constexpr uint kThreads{ 5 };
            constexpr uint kJobs{ kThreads * 2 };

            ThreadPool tp{ kThreads };
            tp.Start();

            std::vector<std::promise<int>> promises{};
            for (auto i = 0; i != kJobs; ++i) {
                promises.emplace_back();
            }
            std::vector<std::future<int>> results{};
            for (auto i = 0; i != kJobs; ++i) {
                auto& job_promise = promises[i];
                results.emplace_back(job_promise.get_future());
                tp.AddJob([i, &job_promise]() {
                    const auto value = DoWork(i);
                    job_promise.set_value(value);
                    });
            }


            for (uint i = 0; i < kJobs; ++i) {
                CHECK(results[i].valid());
                const auto actual_value = results[i].get();
                CHECK(actual_value == DoWork(i));
            }

            tp.Stop();


        }

    }
}